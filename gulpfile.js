var gulp = require('gulp'),
  nodemon = require('gulp-nodemon'),
  livereload = require('gulp-livereload');


gulp.task('develop', function () {
  livereload.listen();
  nodemon({
    script: 'bin/www',
    ext: 'js jade coffee'
  }).on('restart', function () {
    setTimeout(function () {
      livereload.changed(__dirname);
    }, 500);
  });
});


gulp.task('onlyserve', function () {
    nodemon({
        script: 'bin/www',
        ext: 'js'
    }).on('restart', function () {
    });
});

gulp.task('default', [
  'develop'
]);
