/**
 * Created by andreybobrov on 10/29/15.
 */
var helper = {};

helper.getArg = function(args, argName, defaultVal) {
    return ((typeof args[argName] === void 0) || !args[argName]) ? defaultVal : args[argName];
};

module.exports = helper;