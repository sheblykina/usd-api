/**
 * Created by andreybobrov on 12/12/15.
 */
var Q = require("q");

var promiseHelper = {
    waitAll:function(obj_arr, funcName, args) {

        var promise_arr = obj_arr.map(function (item) {
            return item[funcName](args);
        });

        return Q.all(promise_arr);
    },

    chainThem:function(obj_arr, funcName, args){
        //TODO: implement
    }

}

module.exports = promiseHelper;