/**
 * Created by andreybobrov on 7/21/15.
 */
var currencyMapper = require('./valueMappers/currencyMapper');
var typeMapper = require('./valueMappers/typeMapper');

var constructor = {};

constructor.getCacheKey = function(args){
    var id = args._id;
    var city = args.city;
    var currency = currencyMapper.byNumber(args.currency);
    var type = typeMapper.byNumber(args.type);

    if(id){
        return id.toString();
    }

    return '/'+city+'/'+currency +'/'+type;
};

module.exports = constructor;