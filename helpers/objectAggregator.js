/**
 * Created by andreybobrov on 7/20/15.
 */
var aggregator = {};
aggregator.merge = function(obj1, obj2) {

    if(!obj2)
        return obj1;

    if(!obj1)
        return obj2;

    var obj3 = obj1.concat(obj2);
    return obj3;

};

module.exports = aggregator;