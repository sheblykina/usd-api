/**
 * Created by andreybobrov on 7/21/15.
 */
var currencyMapper = Object.create(null);

currencyMapper['usd'] = 0;
currencyMapper['eur'] = 1;
currencyMapper['rur'] = 2;

currencyMapper.keys= Object.keys(currencyMapper);
currencyMapper.byNumber  = function (n){
    return this.keys[n];
};

module.exports = currencyMapper;