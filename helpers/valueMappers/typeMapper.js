/**
 * Created by andreybobrov on 8/31/15.
 */
var typeMapper = Object.create(null);
typeMapper['1'] = 1;
typeMapper['2'] = 2;

typeMapper.keys= Object.keys(typeMapper);
typeMapper.byNumber  = function (n){
    return this.keys[n];
};

module.exports = typeMapper;