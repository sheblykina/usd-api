/**
 * Created by andreybobrov on 10/21/15.
 */
var jsdom = require('jsdom');
var Iconv = require('iconv').Iconv;
// TODO: Think how to utilize iconv-lite in future
var Q = require('q');
var iconvLite = require('iconv-lite');
var $ = function() {};
var service = {};

//TODO: http://habrahabr.ru/post/210166/
service.get = function(url, jquerySelector, decode) {
    var deferred = Q.defer();

    jsdom.env({
        url: url,
        scripts: ["http://code.jquery.com/jquery.js"],
        encoding: 'binary',
        headers: {
            'user-agent': 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
            'Accept-Language': 'en-US,en;q=0.8,ru;q=0.6,uk;q=0.4'
        },
        done: function (errors, window) {
            if (errors) {
                console.error("Error while getting data from source " + url);
                console.error(errors);
                deferred.reject(errors);
            }
            //Ukrainian/Russian characters are encoded in the http
            //so we are using the iconv here to convert the whole
            //page to the readable string.
            // http://habrahabr.ru/post/17640/
            // TODO: Optimize
            var error = null;
            try {
                $ = window.$;
                var bb = $(window.document.body).html();
                var body = new Buffer(bb, 'binary');
                iconv = new Iconv(decode, 'utf-8//TRANSLIT//IGNORE');
                var correctBody = iconv.convert(body).toString();
                //as now we have a correct decoded body,
                //we should find a table.
                //parseHTML is used because iconv returns a raw string.
                var correctTable = $.parseHTML(correctBody);
                var table = $(correctTable).find(jquerySelector);
            }
            catch (e) {
                console.error("caught error during jsdom parsing or request. Ex:");
                console.error(e);
                console.error(e.stack);
                deferred.reject(e);
            }

            if (!table || table.length == 0) {
                deferred.reject("ddos");
            } else {
                //Returning the element we are interested in
                deferred.resolve({$: $, table: table});
            }
        }
    });

    return deferred.promise;
};

module.exports = service;