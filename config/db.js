/**
 * Created by andreybobrov on 4/9/15.
 */
'use strict';
var mongoose = require('mongoose');
var globals = require('../config/globals/config');

var config = globals.mongoConfig;

var port = (config.port.length > 0) ? ":" + config.port : '';
var login = (config.user.length > 0) ? config.user + ":" + config.pw + "@" : '';
var uristring =  "mongodb://" + login + config.host + port + "/" + config.db;

//TODO: make sure to change this in production
// bla bla bla test change
uristring = "mongodb://"+ config.host+port+"/"+config.db;

var mongoOptions = { db: { safe: true } };

// Connect to Database
mongoose.connect(uristring, mongoOptions, function (err, res) {
    if(err){
        console.log('ERROR connecting to: ' + uristring + '. ' + err);
    }else{
        console.log('Successfully connected to: ' + uristring);
    }
});

if(globals.mongoCacheEnabled){
    var cacheOpts = globals.mongoCacheOptions;
    require('mongoose-cache').install(mongoose, cacheOpts)
}


module.exports = mongoose;