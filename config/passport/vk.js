/**
 * Created by andreybobrov on 8/19/15.
 */
var VkStrategy  = require('passport-vkontakte').Strategy;
var vkConfig = require('./authSettings').vk;
var User = require('../../database/models/user');

module.exports = function(passport) {

    passport.use('vkontakte', new VkStrategy({
            clientID     : vkConfig.ID,
            clientSecret  : vkConfig.Secret,
            callbackURL     : vkConfig.callbackUrl,
            profileFields: ['username', 'displayName', 'profileUrl']

        },
        function(token, tokenSecret, profile, done) {

            // make the code asynchronous
            // User.findOne won't fire until we have all our data back from Twitter
            process.nextTick(function() {

                User.findOne({ 'vk.id' : profile.id }, function(err, user) {

                    // if there is an error, stop everything and return that
                    // ie an error connecting to the database
                    if (err)
                        return done(err);

                    // if the user is found then log them in
                    if (user) {
                        return done(null, user); // user found, return that user
                    } else {
                        // if there is no user, create them
                        var newUser                 = new User();

                        // set all of the user data that we need
                        newUser.vk.id          = profile.id;
                        newUser.vk.token       = token;
                        newUser.vk.username    = profile.username;
                        newUser.vk.displayName = profile.displayName;
                        newUser.vk.profileUrl  = profile.profileUrl;

                        // save our user into the database
                        newUser.save(function(err) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });
                    }
                });

            });

        }));

};