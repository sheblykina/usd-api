/**
 * Created by andreybobrov on 8/19/15.
 */
var facebook = require('./facebook');
var twitter = require('./twitter');
var vk = require('./vk');
var User = require('../../database/models/user');

module.exports = function(passport){

    // Passport needs to be able to serialize and deserialize users to support persistent login sessions
    passport.serializeUser(function(user, done) {
        console.log('serializing user: ');console.log(user);
        done(null, user._id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            console.log('deserializing user:',user);
            done(err, user);
        });
    });

    // Setting up Passport Strategies for Facebook, VK and Twitter
    facebook(passport);
    twitter(passport);
    vk(passport);

};