/**
 * Created by andreybobrov on 8/28/15.
 */
var aggregator = require('../helpers/objectAggregator');
var Q = require('q');
var globals = app.globals;

var dataStores  = {
    _arr: [],
    _collection: Object.create(null),

    addSource: function (source) {
        dataStores._arr.push(source);
        dataStores._collection[source.name] = source;
    },

    getFromAll: function (args) {
        var deferred = Q.defer();

        var promise_arr = this._arr.map(function (source) {
            return source.get(args)
        });

        Q.all(promise_arr).then(function (result) {
            var cacheRes = [];

            result.forEach(function (r) {
                cacheRes = aggregator.merge(cacheRes, r);
            });

            deferred.resolve(cacheRes);
        }, function (err) {
            deferred.reject(err);
        });

        return deferred.promise;
    },

    warmUpAll: function () {
        this._arr.forEach(function (a) {
            a.warmUp();
        });
    }
};

var financeIUaSource = require('../caching/concretes/financeIUaDataSource');
dataStores.addSource(financeIUaSource);
var minfinSource = require('../caching/concretes/minfinDataSource');
dataStores.addSource(minfinSource);

dataStores.dbCache = Object.create(null);
dataStores.dbCache.user = {};
dataStores.dbCache.market = {};

module.exports = dataStores;