/**
 * Created by andreybobrov on 8/28/15.
 */

var globals = {};

// Remote datasources warmup settings
globals.warmupOnStart = false;
if(globals.warmupOnStart){
    console.log("[DEBUG]: Warmup on start activated");
}
globals.warmUpFrequency = 1000; // 1 sec

// Remote datasources refresh agent configuration
globals.cacheRefreshInterval = 15*60*1000; // 15 mins
globals.cacheRefreshRequestsFrequency = 1000; // 1 sec
globals.cachePrefillList = require('./cachePrefillList');

// MongoDB configuration
globals.mongoConfig  = {
    "db": "testDb1",
    "host": "localhost",
    "user": "test",
    "pw": "test",
    "port": 27017
};

globals.mongoCacheEnabled = true;
if(!globals.mongoCacheEnabled){
    console.warn("[DEBUG]: Warmup on start activated")
}

globals.mongoCacheOptions = {
    max:50, // Up to 50 entries
    maxAge:1000*60*2 // Lifetime is 2 minutes
};

// CORS Configuration
globals.websiteHostName = 'http://localhost:9000'; // a url for the front-end website
globals.corsWhiteList = [globals.websiteHostName, 'http://127.0.0.1:9000']; //white list for CORS requests

// AUTH / passport stuff
globals.superSecret = 'I have generated this sectet from my onion brain';
globals.jwtToketExpiration = 1440;
globals.adminTwitterId = "55d464a6f38b83f540b4a44b";
globals.adminFbId = "55d45fe74cf10d3b3d88f4b5";
globals.adminVkId = "55d4624cde4b89d83f3b7711";
globals.adminIds = [globals.adminFbId, globals.adminTwitterId, globals.adminVkId];

module.exports = globals;