/**
 * Created by andreybobrov on 8/28/15.
 */


var env = process.env.NODE_ENV || 'development';

var config = {
    development: require('./config.debug'),
    production: require('./config.production')
};

module.exports = config[env];