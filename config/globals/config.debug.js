/**
 * Created by andreybobrov on 8/28/15.
 */

var globals = require('./config.generic');

//
// <!-- DEBUG!!! stuff -->
//

globals.noMongo = false;
if(globals.noMongo){
    console.log('[DEBUG]: Running No-Mongo configuration');
}

globals.cacheAgentTest = false; //should we add a route for testing our cache?
if(globals.cacheAgentTest) {
    globals.cacheRefreshInterval = 15 * 1000; // 15 sec
    globals.cacheRefreshRequestsFrequency = 5000; // 3 sec
    console.log('[DEBUG]: Cache agent testing is enabled with interval = '+ globals.cacheRefreshInterval/1000 + ' sec');
}

// If we need to test spinner on a front-end
globals.responseDelayEnabled = false;
globals.responseDelay = 5000;
if(globals.responseDelayEnabled){
    console.log('[DEBUG]: Response delay is enabled with delay = '+ globals.responseDelay/1000 + ' sec');
}

globals.websiteHostName = 'http://localhost:9000'; // a url for the front-end website
//
// <!-- END DEBUG -->
//

module.exports = globals;