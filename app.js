var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var expressSession = require('express-session');
var cors = require('cors');
var jwt = require('jsonwebtoken');

app = express();


var env = process.env.NODE_ENV || 'development';
app.locals.ENV = env;
app.locals.ENV_DEVELOPMENT = env == 'development';

//Storage for custom global stuff
var globals = app.globals = require('./config/globals/config');
var dataStores = app.dataStores = require('./config/dataStores');

if(globals.warmupOnStart){
    dataStores.warmUpAll();
}

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//
// <!-- PASSPORT Configuration -->
//
app.use(expressSession({secret: globals.superSecret}));

//we should use passport only with a session-storage
if(!globals.noMongo) {
    app.use(passport.initialize());
    app.use(passport.session());
    var initPassport = require('./config/passport/passportConfig');
    initPassport(passport);
    app.passport = passport;
}
else {
    console.log('DEBUG: Authentication is disabled because of NO-MONGO configuration')
}
//
// <!-- END PASSPORT -->
//


//
// <!--CORS Configuration -->
//

app.use('/', function(req, res, next) {
    res.header('Access-Control-Allow-Credentials', "true");
    res.header('Access-Control-Allow-Origin', req.headers.origin);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept');
    if ('OPTIONS' == req.method) {
        res.send(200);
    } else {
        next();
    }
});

//white list -> specifies a list of URLs for CORS validation
//['http://example1.com', 'http://example2.com'];
var whitelist = globals.corsWhiteList;
var corsOptions = {
    origin: function(origin, callback){
        var originIsWhitelisted = whitelist.indexOf(origin) !== -1;
        callback(null, originIsWhitelisted);
    }
};
app.use(cors(corsOptions));
//
// <!--END CORS -->
//

//
//<!-- ROUTES CONFIGURATION -->
//
if (app.globals.responseDelayEnabled) {
    var responseDelay = require('./routes/debug/responseDelay');
    app.use('/', responseDelay);
}

var market = require('./routes/market');

var marketCacheRoute = {};
if(!globals.noMongo) {
    marketCacheRoute = require('./routes/marketCache');
} else {
    marketCacheRoute = require('./routes/debug/marketCacheNoDB');
}
app.use('/market', marketCacheRoute);
app.use('/market', market);

if(!globals.noMongo) {
    var auth = require('./routes/auth');
    app.use('/auth', auth);
}

app.use('/user', require('./routes/user'));

/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

//
//<!-- END ROUTES CONFIGURATION-->
//


/// error handlers

// development error handler
// will print stacktrace

if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.send({
            message: err.message,
            error: err,
            title: 'error'
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {},
        title: 'error'
    });
});


module.exports = app;
