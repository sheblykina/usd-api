/**
 * Created by andreybobrov on 10/29/15.
 */
var Q = require('q');
var cacheAgent = function(storage, refreshInterval) {

    var that = this;

    var interval = this.interval = refreshInterval || app.globals.cacheRefreshInterval;
    var freq = this.frequency = app.globals.cacheRefreshRequestsFrequency;
    var isProcessing = this.isProcessing = false;
    this.active = true;
    var refresh = that.refresh = function () {

        var processEntry = storage.cacheEntryProcessor.processEntry;

        if (!isProcessing) {

            console.log(storage.name + ' Data refresh agent is running');

            var i = freq;
            isProcessing = true;

            var cities = storage.getCityList();
            var types = storage.getTypesList();
            var currencies = storage.getCurrencyList();

            var close = function (city, type, currency) {
                setTimeout(function () {
                    processEntry({city: city, currency: currency, type: type});
                }, i);
                i += freq;
            };

            for (var city in cities) {
                for (var type in types) {
                    for (var currency in currencies) {
                        // this is needed to copy references in closure
                        close(cities[city], types[type], currencies[currency]);
                    }
                }
            }

            //We have processed all the entries
            setTimeout(function () {
                isProcessing = false;
                console.log(storage.name + ' Data refresh agent has finished');
            }, i);
        }
    };

    this.intervalHandle = setInterval(refresh, interval);


    that.stop = function () {
        if (that.active) {
            clearInterval(that.intervalHandle);
        }
    };

    that.triggerOnce = function () {
        if (!that.isProcessing)
            setTimeout(that.refresh, 10);
    };

    that.resume = function (forceStart) {
        var force = forceStart || false;

        if (!cacheAgent.active) {
            that.intervalHandle = setInterval(that.refresh, that.interval);
        }

        if (force) {
            that.triggerOnce();
        }
    };


};

module.exports = cacheAgent;