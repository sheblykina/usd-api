/**
 * Created by andreybobrov on 7/2/15.
 */
var remoteDataService = require('../../helpers/remoteDataService');
var Q = require('q');

var processor = function(storage, onlyNew) {
    var that = this;
    var storage = storage;
    var parser = storage.parser;
    var selector = storage.remoteSelector;
    var onlyNew = onlyNew || false;

    this.processEntry = function (args) {
        var key = storage.getKey(args);
        var uri = storage.getUrl(args);

        remoteDataService.get(uri, selector, storage.decodeEncoding).then(function (result) {
            var $ = result.$;
            var table = result.table;
            var arr = {};

            if (onlyNew) {
                arr = parser.parseTable($, table, storage.latestEntryTime);
            }
            else {
                arr = parser.parseTable($, table);
            }

            storage.storage[key] = arr;
        });
    };

    this.processEntryDefer = function (args, tries, deferred) {
        var deferred = deferred || Q.defer();
        var key = storage.getKey(args);
        var uri = storage.getUrl(args);
        // tries are used for preventing ddos protection of the resource:)
        var tries = tries || 3;

        remoteDataService.get(uri, selector, storage.decodeEncoding).then(function (result) {
                var $ = result.$;
                var table = result.table;
                var arr = {};

                if (onlyNew) {
                    arr = parser.parseTable($, table, storage.latestEntryTime);
                }
                else {
                    arr = parser.parseTable($, table);
                }

                storage.storage[key] = arr;
                deferred.resolve(arr);
            },
            function (err) {
                if (err == "ddos") {
                    tries--;
                    if (tries <= 0) {
                        deferred.reject(err);
                    } else {
                        return that.processEntryDefer(args, tries, deferred);
                    }
                } else {
                    deferred.reject(err)
                }
            });

        return deferred.promise;
    }
};


module.exports = processor;