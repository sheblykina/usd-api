/**
 * Created by andreybobrov on 4/8/15.
 */

//This is a table parser
//Used to parse the html table returned from the i.ua
// some change
var parser = {

    topDocsCount: 25,

    // bottom time bound is used for selecting only latest entries
    parseTable: function ($, table, bottomTimeBound) {
        var rows = [];
        var topDocs = this.topDocsCount;
        var onlyNew = false;
        if(bottomTimeBound) {
            onlyNew = true;
        }
        //By default, the i.ua encodes the phone number using the base64 strings
        //This method decodes the base64 phone number
        var showPhone = function (phone) {
            return new Buffer(phone, 'base64').toString("ascii");
        };

        var getPhone = function ($phoneEl) {
            var $spn = $phoneEl.find('span');
            var phone = $($spn[0]).text();
            var hash = $($spn[1]).attr('onclick');
            hash = hash.substr(hash.length - 14, 12);
            phone += showPhone(hash);
            return phone;
        };

        var $rows = table.find('tbody tr').not('.invalid');
        var iterCount = $rows.length >= topDocs ? topDocs : $rows.length;

        var func = function (i) {
            var $row = $($rows[i]);
            var rowObj = {};
            rows[i] = rowObj;
            var $cells = $row.find('td');
            var cellCount = $cells.length;

            if (cellCount <= 0)
                return;

            rowObj.time = $($cells[0]).text();

            if (onlyNew && rowObj.time < bottomTimeBound) {
                return;
            }

            rowObj.rate = $($cells[1]).text();
            var c = $($cells[2]).text();
            rowObj.count = c.substring(0, c.length - 2);
            rowObj.phone = getPhone($($cells[3]));
            rowObj.place = $($cells[4]).text();
            rowObj.comment = $($cells[5]).text();
        };

        for (var j = 0; j < iterCount; j++) {
            func(j);
        }

        //because we will always have one empty element due to <th>'s
        rows.shift();
        return rows;
    }
};



module.exports = parser;