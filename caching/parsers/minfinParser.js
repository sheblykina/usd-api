/**
 * Created by andreybobrov on 10/25/15.
 */

//This is a table parser
//Used to parse the html table returned from the minfin
var Q = require("q");
var request = require('request');

var parser = {
    topDocsCount: 25,
    ensurePhoneRecursionDeep: 10,
    rowSelector: "div.au-deal-row",
    hiddenPhoneConst: "xxx-x",
    showPhoneUri: "http://minfin.com.ua/modules/connector/connector.php?action=auction-get-contacts&bid=",
    cookieVal: '_ym_uid=1446490003973606708; __utmt=1; _ym_visorc_11388685=w; __gads=ID=157d5b5c71fdfe3c:T=1445454214:S=ALNI_MZ1c_YAsMlrBQeUPOURdcMe2kND9w; 9190053a30=fa90b1a5302c9b4f1b454f63b26b05b201b16ae4; b=b; minfincomua_region=56; __nc_l=2713412721069873226001988071566731988751164575219320251298500704:|S2lldg==; __nc_ws=eyI4NEQ2LTEyQjktOUZDQi0xNjQ3LTFZdEhYaiI6eyJ2YyI6Mzh9fQ==; __utma=73089740.173354260.1445291927.1445760683.1445760683.14; __utmb=73089740.90.8.1445770829024; __utmc=73089740; __utmz=73089740.1445368009.3.3.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); __utmv=73089740.|1=User%20Type=guest=1; mf_r=1445291923.9497.80920; __gads=ID=157d5b5c71fdfe3c:T=1445454214:S=ALNI_MZ1c_YAsMlrBQeUPOURdcMe2kND9w; 9190053a30=fa90b1a5302c9b4f1b454f63b26b05b201b16ae4; b=b; _ym_uid=1446490003973606708; minfincomua_region=56; mf_r=1445291923.9497.80920; __utma=73089740.173354260.1445291927.1446574930.1446576199.27; __utmb=73089740.24.9.1446579043757; __utmc=73089740; __utmz=73089740.1445368009.3.3.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); __utmv=73089740.|1=User%20Type=guest=1; _ym_visorc_11388685=w; __nc_l=2713412721069873226001988071566731988751164575219320251298500704:|S2lldg==; __nc_ws=eyI4NEQ2LTEyQjktOUZDQi0xNjQ3LTFZdEhYaiI6eyJ2YyI6NzB9fQ==',
    retries: {},
    $: function () {
    },

    showPhone: function (bidId) {
        var that = this;
        var deferred = Q.defer();
        var options = {
            url: 'http://minfin.com.ua/modules/connector/connector.php?action=auction-get-contacts&r=true&bid=' + (parseInt(bidId) + 1).toString(),
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.71 Safari/537.36',
                'Connection': 'keep-alive',
                'Cookie': that.cookieVal
                //'Host': 'minfin.com.ua',
                //'Origin': 'chrome-extension://aicmkgpgakddgnaphhhpliifpcfhicfo',
                //'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                //'Referer': 'http://minfin.com.ua/currency/auction/usd/sell/dnepropetrovsk',
                //'Content-Length': '11',
                //'Postman-Origin': 'http://minfin.com.ua',
                //'Accept-Language': 'en-US,en;q=0.8,ru;q=0.6,uk;q=0.4',
                //'Accept-Encoding': 'gzip, deflate',
                //'X-DevTools-Emulate-Network-Conditions-Client-Id': '26A938ED-BCEC-49D1-B740-A4F9C89F888D',
                //'X-FirePHP-Version': '0.0.6',

            },
            body: 'bid=' + bidId + "&r=true&action=auction-get-contacts"
        };

        request.post(options, function (err, res, body) {
            if (body.indexOf("<") >= 0 || err) {
                deferred.reject({bidId: bidId});
            } else {
                deferred.resolve({bidId: bidId, body: JSON.parse(body)});
            }
        });

        return deferred.promise;
    },

    ensurePhones: function (tries) {
        var that = this;
        var tries = tries || this.ensurePhoneRecursionDeep;

        var innerPhonesIterator = function () {
            var deffered = Q.defer();
            var promises = [];

            for (var bidId in that.retries) {
                promises.push(that.showPhone(bidId));
            }

            Q.allSettled(promises).then(function (results) {
                var bool = true;
                results.forEach(function (result) {
                    if (result.state === "fulfilled") {
                        var value = result.value;
                        var bidData = that.retries[value.bidId];
                        bidData.phoneElRef.phone = bidData.hashedPhone
                                                            .replace(that.hiddenPhoneConst, value.body['data'])
                                                                .trim().replace(/-/g, "");
                        delete that.retries[value.bidId];
                    } else {
                        bool = false;
                    }
                });


                if (bool) {
                    deffered.resolve();
                } else {
                    deffered.reject();
                }

            });

            return deffered.promise;
        };

        innerPhonesIterator().then(function () {
            // TODO: make it configurable like verbose/silent
            console.log("DEBUG: Minfin Parser: all phones resolved. tries left: " + tries);
            return;
        }, function () {
            tries--;
            if (tries <= 0) {
                console.error("Minfin Parser: ensurePhones run out of tries");
                return;
            }
            that.ensurePhones(tries);
        });
    },

    getPhone: function ($phoneEl, rowDOMPhoneReference) {
        var $ = this.$;

        var phone = "+38 " + $($phoneEl).text();//.trim().replace(/-/g, "");
        var bidId = $(".js-showPhone", $phoneEl).attr('data-bid-id');

        this.retries[bidId] = {phoneElRef: rowDOMPhoneReference, hashedPhone: phone};
    },

    parseTable: function ($, table, bottomTimeBound) {
        this.$ = $;
        var that = this;
        var rows = [];
        var topDocs = this.topDocsCount;
        var onlyNew = false;
        if (bottomTimeBound) {
            onlyNew = true;
        }

        var $rows = table.find(this.rowSelector);
        var iterCount = $rows.length >= topDocs ? topDocs : $rows.length;

        var func = function (i) {
            var $row = $($rows[i]);
            var rowObj = {};
            rowObj.time = $("small.au-deal-time", $row).text();

            if (onlyNew && rowObj.time < bottomTimeBound) {
                return;
            }

            rowObj.rate = $("span.au-deal-currency", $row).text();
            var c = $("span.au-deal-sum", $row).text();
            rowObj.count = c.substring(0, c.length - 1);
            rowObj.phone = "";
            that.getPhone($("span.au-dealer-phone", $row), rowObj);
            rowObj.comment = $("span.au-deal-msg", $row).text();
            rows.push(rowObj);
        };

        for (var i = 0; i < iterCount; i++) {
            func(i);
        }

        this.ensurePhones();
        //because we will always have one empty element due to <th>'s
        //rows.shift();
        return rows;
    }
};

module.exports = parser;