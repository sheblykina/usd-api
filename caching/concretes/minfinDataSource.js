/**
 * Created by andreybobrov on 11/2/15.
 */
var DataSource = require('../dataSource');
var entryProcessor = require('../workers/entryProcessor');
var parser = require('../parsers/minfinParser');
var argsHelper = require('../../helpers/functionArgsHelper');

var minfin = new DataSource("Minfin", "div.au-deals-list", parser);

minfin.getKey =function(args) {
    var city = argsHelper.getArg(args, 'city', 'kiev').toLowerCase();
    var currency = argsHelper.getArg(args, 'currency', 'usd').toLowerCase();
    var type = argsHelper.getArg(args, 'type', '1').toLowerCase();
    return city + "/" + currency + "/" + type;
};

minfin.getUrl = function(args) {
    var city = argsHelper.getArg(args, 'city', 'kiev').toLowerCase();
    var currency = argsHelper.getArg(args, 'currency', 'usd').toLowerCase();
    var type = argsHelper.getArg(args, 'type', '1').toLowerCase();
    var minfinType = type == '1' ? 'buy' : 'sell';
    //http://minfin.com.ua/currency/auction/usd/buy/dnepropetrovsk/?sort=time&order=desc
    return 'http://minfin.com.ua/currency/auction/' + currency + "/" + minfinType + '/' + city + '/?sort=time&order=desc';
};

minfin.cacheEntryProcessor = new entryProcessor(minfin, true);
minfin.decodeEncoding = 'utf-8';
minfin.initAgent();

module.exports = minfin;