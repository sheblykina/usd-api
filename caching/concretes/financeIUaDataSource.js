/**
 * Created by andreybobrov on 10/29/15.
 */
var DataSource = require('../dataSource');
var entryProcessor = require('../workers/entryProcessor');
var parser = require('../parsers/financeParser');
var argsHelper = require('../../helpers/functionArgsHelper');

var financeIua = new DataSource("Finance I Ua", "table", parser);

financeIua.getKey =function(args) {
    var city = argsHelper.getArg(args, 'city', 'kiev').toLowerCase();
    var currency = argsHelper.getArg(args, 'currency', 'usd').toLowerCase();
    var type = argsHelper.getArg(args, 'type', '1').toLowerCase();
    return city + "/" + currency + "/" + type;
};

financeIua.getUrl = function(args) {
    var city = argsHelper.getArg(args, 'city', 'kiev').toLowerCase();
    var currency = argsHelper.getArg(args, 'currency', 'usd').toLowerCase();
    var type = argsHelper.getArg(args, 'type', '1').toLowerCase();
    return 'http://finance.i.ua/market/' + city + "/" + currency + '/?sort=time&desc=1&type=' + type;
};

financeIua.decodeEncoding = 'windows-1251';
financeIua.cacheEntryProcessor = new entryProcessor(financeIua, true);
financeIua.initAgent();

module.exports = financeIua;