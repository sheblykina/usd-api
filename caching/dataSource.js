/**
 * Created by andreybobrov on 10/29/15.
 */
<!-- Cache abstraction layer description -->
//
// Cache -> storage: a collection of entries: [key] = 'data'
//
// Cache -> refreshAgent: an agent that performs routine to refresh the cache actual data
//                        it iterates over entries calling the cacheEntryProcessor's.process() method
//                        interval parameter is used for specifying the cache refresh frequency
//
// Cache -> cacheEntryProcessor: a processor which knows how to request the entry's url correctly
//
// Cache -> crawler: a crawler that will warm-up the cache depending on the parameters
//
// Cache -> getKey(city, currency, type): function returning cache key
//
// Cache -> getUrl(key): function returning entry url based on the cache key
var Q = require('q');
var remoteDataService = require('../helpers/remoteDataService');
var refreshAgent = require('./workers/refreshAgent');

var dataSource = function(name, remoteSelector, parser){
    this.name = name;
    this.remoteSelector = remoteSelector;
    this.parser = parser;
    this.storage = Object.create(null);
    this.decodeEncoding = "";
    this.latestEntryTime = "00:00";
    this.cityList = app.globals.cachePrefillList;
};

dataSource.prototype.getTypesList = function(){ return['1', '2']; };

dataSource.prototype.getCurrencyList = function(){ return['usd','eur', 'rub']; };

dataSource.prototype.getCityList = function(){ return this.cityList; };

dataSource.prototype.getKey = function(args){ };

dataSource.prototype.getUrl = function(args){ };

dataSource.prototype.cacheEntryProcessor = {};

dataSource.prototype.refreshInterval = app.globals.cacheRefreshInterval;

dataSource.prototype.initAgent = function() {
    this.refreshAgent =  new refreshAgent(this, this.refreshInterval);
};

dataSource.prototype.cleanup = function(){


}

dataSource.prototype.get = function(args) {
    var deferred = Q.defer();

    var key = this.getKey(args);

    var entry = this.storage[key];

    if (entry) {
        setTimeout(function () {
            deferred.resolve(entry)
        }, 0);

        return deferred.promise;
    }
    else {
        return this.getFromRemote(args);
    }
};
// https://coderwall.com/p/ijy61g/promise-chains-with-node-js
// Crawler -> http://habrahabr.ru/post/210166/
dataSource.prototype.getFromRemote = function(args){
    var that = this;
    var deferred = Q.defer();

    var url = this.getUrl(args);
    var key = this.getKey(args);

    remoteDataService.get(url, this.remoteSelector, this.decodeEncoding).then(function(data){
        var $ = data.$;
        var table = data.table;

        var arr = that.parser.parseTable($, table);

        that.storage[key] = arr;
        that.latestEntryTime = arr[arr.length-1].time;

        deferred.resolve(arr);
    }, function(err){
        deferred.reject(err);

    });
    return deferred.promise;
};

dataSource.prototype.warmUp = function() {
    var that = this;
    var processEntry = this.cacheEntryProcessor.processEntryDefer;

    console.log(this.name + ' Warm up has been started');

    var cities = this.getCityList();
    var types = this.getTypesList();
    var currencies = this.getCurrencyList();

    var close = function(city, type, currency) {
        promise_chain = promise_chain.then(
            function () {
                console.info("[DEBUG]: " + that.name + " Warmup. Processing city: " + cities[city] + " type:" + type + " currency: " + currencies[currency]);
                return processEntry({
                    city: cities[city],
                    type: types[type],
                    currency: currencies[currency]
                });
            });
    };

    var promise_chain = Q.resolve();

    for (var city in cities) {
        for (var type in types) {
            for (var currency in currencies) {
                // this is needed to copy references in closure
                close(city, type, currency);

            }
        }
    }

    promise_chain = promise_chain.then(function () {
        console.log(that.name + ' Warm up has been completed');
    });
    return promise_chain;
};

module.exports = dataSource;