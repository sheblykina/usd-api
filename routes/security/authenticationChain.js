/**
 * Created by andreybobrov on 10/27/15.
 */

var globals = app.globals;
var isAuthenticatedOne = function (req, res, next) {
    // if user is authenticated in the session, call the next() to call the next request handler
    // Passport adds this method to request object. A middleware is allowed to add properties to
    // request and response objects
    if (req.isAuthenticated())
        return next();
    // if the user is not authenticated then redirect him to the login page
    return res.status(403).send({
        success: false,
        message: 'User is not authenticated'
    });
};

var isAuthenticatedTwo = function(req,res, next) {
    var token = req.cookies.sptoken;
    if (token) {
        jwt.verify(token, app.globals.superSecret, function (err, decoded) {
            if (err) {
                return res.status(403).send({
                    success: false,
                    message: 'Invalid token'
                });
            } else {
                req.customData = req.customData || {};
                req.customData.decodedUser = decoded;
                next();
            }
        });
    }
    else {
        // if there is no token
        // return an error
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });

    }
};

var isAdmin = function(req,res,next) {
    var adminIds = globals.adminIds;
    var user = customData.decodedUser || req.user._doc;
    var userId = user._id;
    if (adminIds.indexOf(userId) < 0) {

        console.error(">>>>>> Someone was trying to access the admin page without permissions");
        // TODO: log this bastard to the mongo
        return res.status(403).send({
            success: false,
            message: 'Protected.'
        });
    }
};

var map = { };
map['admin'] = [isAuthenticatedOne, isAuthenticatedTwo, isAdmin];
map['simple'] = [isAuthenticatedOne, isAuthenticatedTwo];

module.exports = map;