/**
 * Created by andreybobrov on 10/27/15.
 */
var express = require('express');
var router = express.Router();
var MarketData = require('../database/models/marketData');
var authChain = require('./security/authenticationChain')['admin'];


router.get('/buildCache', authChain, function(req, res) {

});

module.exports = router;
