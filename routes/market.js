/**
 * Created by andreybobrov on 4/7/15.
 */

var express = require('express');
var router = express.Router();
var TableParser = require('../caching/parsers/financeParser');
var aggregator = require('../helpers/objectAggregator');
var remoteDataService = require('../helpers/remoteDataService');

router.get('/:city?/:currency?/:type?', function(req, res) {

    var city = req.params.city ? req.params.city.toLowerCase() : 'kiev';
    var currency = req.params.currency ? req.params.currency.toLowerCase() : 'usd';
    var type = req.params.type ? req.params.type.toLowerCase() : '2';

    var cache = req.app.dataStores.financeIUa;
    var path = { city:city, currency:currency, type:type };

    var key = cache.getKey(path);
    var uri = cache.getUrl(path);

    remoteDataService.get(uri, 'table', cache.decodeEncoding).then(function(result) {
        var $ = result.$;
        var table = result.table;
        //Iterates over the table and gets the related data (phone, currency amount, etc.)
        var parser = new TableParser($);
        var arr = parser.parseTable(table);

        cache.storage[key] = arr;

        if (req.dbRes) {
            res.json(aggregator.merge(arr, req.dbRes));
        } else {
            res.json(arr);
        }
    });
});

module.exports = router;
