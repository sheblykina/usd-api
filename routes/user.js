/**
 * Created by andreybobrov on 9/3/15.
 */
var express = require('express');
var router = express.Router();
var jwt  = require('jsonwebtoken');
var MarketData = require('../database/models/marketData');
var currencyMapper = require('../helpers/valueMappers/currencyMapper');
var typeMapper = require('../helpers/valueMappers/typeMapper');

var isAuthenticatedOne = function (req, res, next) {
    // if user is authenticated in the session, call the next() to call the next request handler
    // Passport adds this method to request object. A middleware is allowed to add properties to
    // request and response objects
    if (req.isAuthenticated())
        return next();
    // if the user is not authenticated then redirect him to the login page
    return res.status(403).send({
        success: false,
        message: 'User is not authenticated'
    });
};

var isAuthenticatedTwo = function(req,res, next) {
    var token = req.cookies.sptoken;
    if (token) {
        jwt.verify(token, app.globals.superSecret, function (err, decoded) {
            if (err) {
                return res.status(403).send({
                    success: false,
                    message: 'Invalid token'
                });
            } else {
                req.customData = req.customData || {};
                req.customData.decodedUser = decoded;
                next();
            }
        });
    }
    else {
        // if there is no token
        // return an error
        return res.status(403).send({
            success: false,
            message: 'No token provided.'
        });

    }
};

var authChain = [isAuthenticatedOne, isAuthenticatedTwo];


//test if user is authenticated
router.get('/current', authChain, function(req, res) {
    res.send({user: req.customData.decodedUser});
});

router.get('/', authChain, function(req, res) {
    var user = req.customData.decodedUser;
    var userId = user._id;

    MarketData.find({author:userId }).cache().then(function(result){
        res.send(result);
    });
});

// route for posting single entry
// TODO: prettify and make sure to return needed result
router.post('/:id?', authChain, function(req, res) {
    var customData = req.customData||{};
    var user = customData.decodedUser||req.user._doc;
    var userId = user._id;
    var id = req.params.id;
    var data = req.body;
    var time = new Date();
    var obj = {
        author: userId,
        city: data.city,
        currency: currencyMapper[data.currency],
        type: typeMapper[data.tradeType],
        time: time.getHours()+":"+time.getMinutes(),
        rate: data.rate,
        count: data.count,
        phone: data.phone,
        place: data.place || "",
        comment: data.comment || "",
        topBlock: data.topBlock || ""
    };
    var toDB = new MarketData(obj);

    toDB.save(function (error) {
        if(error){
            res.json(error);
        }

        res.json({});
    });
});


//router.get('/token', [isAuthenticated], function(req, res) {
//
//    var token = jwt.sign(req.user, app.globals.superSecret, {
//        expiresInMinutes: 1440 // expires in 24 hours
//    });
//    res.json({user:req.user, token:token});
//});


module.exports = router;
