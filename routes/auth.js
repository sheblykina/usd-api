var express = require('express');
var router = express.Router();
var passport = app.passport;
var jwt  = require('jsonwebtoken');

var pingFrontend = function(req, res) {
    var token = jwt.sign(req.user, app.globals.superSecret, {
        expiresInMinutes: app.globals.jwtToketExpiration || 24 * 60
    });

    res.cookie('sptoken', token, {
        maxAge: 60 * 60 * 1000, httpOnly: true,
        secure: false
    });

    res.redirect((app.globals.websiteHostName || '') + '/#login/auth_close');
};

router.get('/vk',
    passport.authenticate('vkontakte'));

router.get('/vk/callback',
    passport.authenticate('vkontakte'), pingFrontend);

router.get('/fb',
    passport.authenticate('facebook', { scope : 'email' }));

router.get('/fb/callback',
    passport.authenticate('facebook'), pingFrontend);

router.get('/twitter',
    passport.authenticate('twitter'));

router.get('/twitter/callback',
    passport.authenticate('twitter'), pingFrontend);


module.exports = router;
