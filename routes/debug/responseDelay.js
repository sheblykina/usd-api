/**
 * Created by andreybobrov on 9/22/15.
 */
var middleware = function(req, res, next) {
    if (req.app.globals.responseDelayEnabled) {
        setTimeout(function () {
            next()
        }, req.app.globals.responseDelay)
    }
    else{
        next();
    }
};

module.exports = middleware;
