/**
 * Created by andreybobrov on 8/31/15.
 */
/**
 *
 * This is a middleware for returning previously cached stuff
 *
 */

var express = require('express');
var router = express.Router();

router.get('/:city?/:currency?/:type?', function(req, res, next) {

    var city = req.params.city ? req.params.city.toLowerCase() : 'kiev';
    var currency = req.params.currency ? req.params.currency.toLowerCase() : 'usd';
    var type = req.params.type ? req.params.type.toLowerCase() : '2';

    var path = {city: city, currency: currency, type: type};

    var cache = req.app.dataStores.financeIUa;
    var key = cache.getKey(path);

    var entry = cache[key];
    var dbres = null;
    if (entry) {
        res.json(entry);
    }
    else {
        req.dbRes = dbres;
        next();
    }
});

module.exports = router;
