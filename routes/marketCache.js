/**
 *
 * This is a middleware for returning previously cached stuff
 *
 */

var express = require('express');
var router = express.Router();

var MarketData = require('../database/models/marketData');
var aggregator = require('../helpers/objectAggregator');
var currencyMapper = require('../helpers/valueMappers/currencyMapper');
var typeMapper = require('../helpers/valueMappers/typeMapper');

router.get('/:city?/:currency?/:type?', function(req, res, next) {

    var city = req.params.city ? req.params.city.toLowerCase() : 'kiev';
    var currency = req.params.currency ? req.params.currency.toLowerCase() : 'usd';
    var type = req.params.type ? req.params.type.toLowerCase() : '2';

    var path = {city: city, currency: currency, type: type};

    req.app.dataStores.getFromAll(path).then(function (cacheResult) {

        MarketData.find({city: city, currency: currencyMapper[currency], type: typeMapper[type]})
            .select('city type currency time rate count phone place comment topBlock')
            .cache()
            .exec(function (err, result) {

                var dbres = result.length > 0 ? result : null;
                var entry = cacheResult;

                if (entry) {
                    res.json(aggregator.merge(entry, dbres));
                }
                else {
                    req.dbRes = dbres;
                    next();
                }

            });
    });
});

module.exports = router;
