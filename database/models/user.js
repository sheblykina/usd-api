/**
 * Created by andreybobrov on 8/19/15.
 */
var userSchema = require('../schemas/user');
var mongoose = require('../../config/db');

var User = mongoose.model('User', userSchema);
module.exports = User;