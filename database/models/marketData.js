/**
 * Created by andreybobrov on 7/6/15.
 */
var marketDataSchema = require('../schemas/marketData');
var mongoose = require('../../config/db');

var MarketData = mongoose.model('MarketData', marketDataSchema);
module.exports = MarketData;