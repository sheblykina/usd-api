/**
 * Created by andreybobrov on 7/6/15.
 */
var mongoose = require('../../config/db');
var cacheKey = require('../../helpers/cacheKeyConstructor');

var Schema = mongoose.Schema;

var marketDataSchema = new Schema({
    //_id: ObjectId,
    city: String,
    // To make it faster a bit than the string comparison
    // 0 = USD  1 = EUR  2 = RUR  3 = Other
    currency: {type: Number, min: 0, max: 3},
    // 1 - wtb   2 - wts
    type: {type: Number, min: 1, max: 2},
    time: String,
    rate: Number,
    count: Number,
    phone: String,
    place: String,
    comment: String,
    topBlock: Boolean,
    author: String
});

marketDataSchema.pre('save', function(next) {
    var that = this;
    var cache = app.dataStores.dbCache;

    if (app.globals.mongoDebug) {
        console.log('saving() modelData: ' + JSON.stringify(that));
    }

    //clear various dependent cache entries
    var path = cacheKey.getCacheKey({city: that.city, currency: that.currency, type: that.type});
    delete cache.market[path];
    delete cache.user[that.author];

    next();
});


marketDataSchema.pre('find', function(next){
    var cache = app.dataStores.dbCache;
    var args = this._conditions;

    var result;
    if (args.author) {
        result = cache.user[args.author];
    }
    else {
        var key = cacheKey.getCacheKey(args);
        result = cache.market[key];

    }

    //TODO: this won't work. Think about how we can stop the query execution
    // http://docs.mongodb.org/manual/reference/method/db.killOp/#db.killOp
    result = null;
    if(!result){
        next();
    }
});


marketDataSchema.post('find', function(result) {

    var cache = app.dataStores.dbCache;
    var args = this._conditions;

    if (result != undefined && result.count > 0) {
        if (args.author) {
            cache.user[args.author] = result;
        }
        else {
            var key = cacheKey.getCacheKey(args);
            cache.market[key] = result;

        }
    }

    if (app.globals.mongoDebug) {
        console.log('find() returned ' + JSON.stringify(result));
        // prints number of milliseconds the query took
        console.log('find() took ' + (Date.now() - this.start) + ' millis');
    }

});

module.exports = marketDataSchema;