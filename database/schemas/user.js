/**
 * Created by andreybobrov on 8/19/15.
 */
var mongoose = require('../../config/db');

var Schema = mongoose.Schema;

module.exports = new Schema ({
    fb: {
        id: String,
        access_token: String,
        firstName: String,
        lastName: String,
        email: String
    },
    twitter: {
        id: String,
        token: String,
        username: String,
        displayName: String,
        lastStatus: String
    },
    vk: {
        id: String,
        token: String,
        username: String,
        displayName: String,
        profileUrl: String
    }
});